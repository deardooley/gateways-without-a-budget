var TACC = (function() {

  function onWindowResize(e) {
    var zoom = document.querySelector('.reveal > .slides').style.zoom;
    document.querySelector('.tacc-footer').style.zoom = zoom;
  }
  window.addEventListener( 'resize', onWindowResize, false );

  var stack = document.querySelectorAll('.stack .rotate');
  for (var i = 0; i < stack.length; i++) {
    var deg = stack[i].dataset.rotate;
    stack[i].style.webkitTransform = 'rotate(' + deg + 'deg)';
  }

})();
